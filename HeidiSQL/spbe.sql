-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for spbe
CREATE DATABASE IF NOT EXISTS `spbe` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `spbe`;

-- Dumping structure for table spbe.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'lainnya', 'lainnya', NULL, NULL),
	(6, 'Kategori Satu', 'kategori-satu', '2022-01-17 15:03:04', '2022-01-17 15:03:04'),
	(7, 'Kategori 2', 'kategori-2', '2022-01-19 02:11:20', '2022-01-19 02:11:20');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table spbe.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_post_id_foreign` (`post_id`),
  CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table spbe.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.migrations: ~13 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_07_25_134316_create_posts_table', 1),
	(4, '2016_07_27_042414_alter_posts_add_published_at_column', 1),
	(5, '2016_07_28_093436_create_categories_table', 1),
	(6, '2016_07_28_093634_alter_posts_add_category_id_column', 1),
	(7, '2016_07_29_003557_alter_users_add_slug_column', 1),
	(8, '2016_07_29_021600_alter_users_add_bio_column', 1),
	(9, '2016_07_30_015428_alter_posts_add_view_count_column', 1),
	(10, '2016_11_03_023303_add_soft_deletion_to_posts_table', 1),
	(11, '2017_01_31_061501_laratrust_setup_tables', 1),
	(12, '2017_04_06_043225_create_tags_table', 1),
	(13, '2017_08_30_024106_create_comments_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table spbe.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table spbe.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.permissions: ~5 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'crud-post', NULL, NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(2, 'update-others-post', NULL, NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(3, 'delete-others-post', NULL, NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(4, 'crud-category', NULL, NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(5, 'crud-user', NULL, NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table spbe.permission_role
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.permission_role: ~7 rows (approximately)
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(1, 2),
	(4, 2);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping structure for table spbe.permission_user
CREATE TABLE IF NOT EXISTS `permission_user` (
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.permission_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;

-- Dumping structure for table spbe.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`),
  KEY `posts_author_id_foreign` (`author_id`),
  KEY `posts_category_id_foreign` (`category_id`),
  CONSTRAINT `posts_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.posts: ~2 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `author_id`, `title`, `slug`, `body`, `image`, `created_at`, `updated_at`, `published_at`, `category_id`, `view_count`, `deleted_at`) VALUES
	(2, 2, 'Artikel Pertama', 'artikel-pertama', 'ini adalah artikel pertama tanpa gambar', NULL, '2022-01-19 01:26:54', '2022-01-19 02:21:47', '2022-01-19 00:00:00', 6, 7, NULL),
	(3, 1, 'Artikel Kedua', 'artikel-kedua', 'Postingan kedua Menggunakan gambar\r\n\r\n![](/img/gambarbody1.jpg)', 'gambar1.jpg', '2022-01-19 01:31:09', '2022-01-19 01:54:01', '2022-01-19 00:00:00', 6, 18, NULL);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table spbe.post_tag
CREATE TABLE IF NOT EXISTS `post_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.post_tag: ~43 rows (approximately)
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
INSERT INTO `post_tag` (`post_id`, `tag_id`) VALUES
	(1, 6),
	(2, 7),
	(2, 8),
	(2, 9),
	(3, 8),
	(3, 10),
	(4, 2),
	(5, 2),
	(6, 2),
	(6, 4),
	(7, 1),
	(7, 4),
	(8, 2),
	(8, 3),
	(10, 1),
	(10, 3),
	(11, 3),
	(12, 3),
	(13, 2),
	(13, 4),
	(14, 1),
	(15, 1),
	(15, 2),
	(16, 4),
	(17, 1),
	(17, 3),
	(18, 1),
	(19, 2),
	(20, 3),
	(21, 4),
	(23, 3),
	(25, 1),
	(27, 2),
	(28, 1),
	(28, 3),
	(28, 4),
	(29, 1),
	(30, 1),
	(30, 3),
	(32, 1),
	(32, 2),
	(33, 2),
	(34, 1),
	(34, 3),
	(35, 1),
	(36, 1),
	(36, 4),
	(37, 5);
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;

-- Dumping structure for table spbe.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Admin', NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(2, 'adminopd', 'Admin OPD', NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(3, 'member', 'Member', NULL, '2022-01-16 23:29:24', '2022-01-16 23:29:24');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table spbe.role_user
CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.role_user: ~6 rows (approximately)
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
	(1, 1, 'App\\User'),
	(2, 2, 'App\\User'),
	(2, 5, 'App\\User'),
	(3, 3, 'App\\User'),
	(3, 4, 'App\\User'),
	(3, 6, 'App\\User');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;

-- Dumping structure for table spbe.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.tags: ~9 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'PHP', 'php', '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(2, 'Laravel', 'Laravel', '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(3, 'Symphony', 'symphony', '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(4, 'Vue JS', 'vuejs', '2022-01-16 23:29:24', '2022-01-16 23:29:24'),
	(5, '', '', '2022-01-17 09:05:00', '2022-01-17 09:05:00'),
	(6, 'Tag1', 'tag1', '2022-01-19 01:21:34', '2022-01-19 01:21:34'),
	(7, 'Tag 1', 'tag-1', '2022-01-19 01:26:54', '2022-01-19 01:26:54'),
	(8, 'Tag 2', 'tag-2', '2022-01-19 01:31:09', '2022-01-19 01:31:09'),
	(9, 'Tag 3', 'tag-3', '2022-01-19 02:09:50', '2022-01-19 02:09:50'),
	(10, 'Internet', 'internet', '2022-01-19 02:11:13', '2022-01-19 02:11:13');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table spbe.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table spbe.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `slug`, `bio`) VALUES
	(1, 'Super Admin', 'admin', 'superadmin@test.com', '$2y$10$8zT1WBUSdxF3BIEdG7uW6OJVrfvkNlV0gjvr9Rgkp/pf/vMgTsQty', 'FarQQRyTeeRMwwbKindPVVsPBVxNjMgIYZ8KMr3CXz2zLWmMGuAcmMNFh7So', NULL, '2022-01-19 02:04:02', 'super-admin', 'Super Admin\r\n\r\nRole Everything'),
	(2, 'Admin OPD', 'adminopd', 'adminopd@test.com', '$2y$10$1kU76F.T9xviN6T6u2FeZOMiRmrKYGJmwui.FSUjeMIJPUW4IXjYq', 'CLX49UdF9oMg5cDZkPBcqB2X5hIaQryhyeET1vULXp15h1daTfhbtwdUq2OK', NULL, '2022-01-19 02:07:18', 'admin-opd', NULL),
	(3, 'Member', 'member', 'member@test.com', '$2y$10$kdZlqy9LN3VvN9U12lBjXOGMGakmRubRgfnvV7JC83nqRQYc7NM0S', 'AUnNJKl6mk3verZRZReqPRXr74MkZry8iL0XUxBNW2Nx4v5ZOTKgVBNRjJZC', NULL, NULL, 'member', ''),
	(4, 'member dua', 'member2', 'member2@test.com', '$1$EX56GANx$SPZcL/6d5zYaVJTqNB6hY.', 'qcUnGBzWBf4w6Ngu62mIGRPPMoNfu38v6aMHGNYVokoswFIdvlHLf8mTIRfr', '2022-01-16 23:49:06', '2022-01-16 23:49:06', 'member-2', NULL),
	(5, 'Admin OPD 2', 'adminopd2', 'adminopd2@test.com', '$1$hzpMC.ZS$LEv/D6n77z3KgNk.ZrZGK/', NULL, '2022-01-19 01:29:36', '2022-01-19 01:29:36', 'admin-opd-2', ''),
	(6, 'member 3', 'member3', 'member3@test.com', '$1$kdVIYOPx$Hu81GctGtrAC1QWSlarVc1', 'dDreBBgZ7uSLKPNHxkBS79WmnHmSLuEROfincJofQOpWUCZpnJVg5WZKLaOv', '2022-01-19 03:39:56', '2022-01-19 03:39:56', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
